//------------------------------PfPCS---------------------------------------
//----------------------------Labwork #4------------------------------------
//-------OpenMP.Locks, Barriers, Critical Sections--------------------------
//--------------------------------------------------------------------------
//---------------Task: A = (B*MO)*(MT*MR)-d*Z-------------------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 11.04.2018--------------------------------------------------
//--------------------------------------------------------------------------

#include "data.h"
#include <iostream>
#include <omp.h>


void input_Integer(int &a)
{
	a = 1;
}

void input_Vector(Vector &A)
{
	for (int i = 0; i < N; i++)
	{
		A[i] = 1;
	}
}

void input_Matrix(Matrix &MA)
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			MA[i][j] = 1;
		}
	}
}

void output_Vector(const Vector &A)
{
	if (N < 10)
	{
		std::cout << std::endl;
		for (int i = 0; i < N; i++)
		{
			std::cout << A[i] << " ";
		}
		std::cout << std::endl;
	}
}

Vector multiply_Matrix_Matrix_Vector(const Matrix &MA, const Matrix &MB, const Vector &A, const int k)
{
	int cell;
	Matrix matr(N, N);
	Vector result(N);
#pragma omp parallel for
	for (int i = H * k; i < H*(1 + k); i++)
	{
		for (int j = 0; j < N; j++)
		{
			cell = 0;
			for (int k = 0; k < N; k++)
			{
				cell += MA[i][k] * MB[k][j];
			}
			matr[i][j] = cell;
		}
		cell = 0;
		for (int j = 0; j < N; j++)
		{
			cell += A[i] * matr[i][j];
		}
		result[i] = cell;
	}
	return result;
}

Vector multiply_Vector_Matrix(const Matrix &MA, const Vector &A, const int k)
{
	int cell;
	Vector result(N);
#pragma omp parallel for
	for (int i = H * k; i < H*(1 + k); i++)
	{
		cell = 0;
		for (int j = 0; j < N; j++)
		{
			cell += A[j] * MA[j][i];
		}
		result[i] = cell;
	}
	return result;
}

Vector multiply_Vector_Integer(const Vector &A, const int a, const int k)
{
	Vector result(N);
#pragma omp parallel for
	for (int i = H * k; i < H*(1 + k); i++)
	{
		result[i] = A[i] * a;
	}
	return result;
}

void sub_Vectors(const Vector &A, const Vector &B, Vector &C, const int k)
{
#pragma omp parallel for
	for (int i = H * k; i < H*(1 + k); i++)
	{
		C[i] = A[i] - B[i];
	}
}