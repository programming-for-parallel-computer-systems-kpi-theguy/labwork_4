//------------------------------PfPCS---------------------------------------
//----------------------------Labwork #4------------------------------------
//-------OpenMP.Locks, Barriers, Critical Sections--------------------------
//--------------------------------------------------------------------------
//---------------Task: A = (B*MO)*(MT*MR)-d*Z-------------------------------
//--------------------------------------------------------------------------
//---- - Author : Butskiy Yuriy, IO - 52 group------------------------------
//---- - Date : 11.04.2018--------------------------------------------------
//--------------------------------------------------------------------------

#include <iostream>
#include <omp.h>
#include "data.h"
#include <string>

int N = 4;
int P = 4;

int d;
Vector A(N), B(N), Z(N);
Matrix MO(N, N), MT(N, N), MR(N, N);

omp_lock_t key_Lock;

int main()
{
	cout << "Labwork #4 started" << endl;

	omp_set_nested(1);
	omp_set_dynamic(1);
	omp_set_num_threads(P);
	omp_init_lock(&key_Lock);

#pragma omp parallel
	{
		int Tid = omp_get_thread_num();
		string str;
		string endstr;
		str.append("T").append(to_string(Tid + 1)).append("started");
		endstr.append("T").append(to_string(Tid + 1).append("finished"));

		cout << str << endl;

		//1. If Tid = 0,  input values of MR, d
		if (Tid == 0)
		{
			input_Integer(d);
			input_Matrix(MR);
		}
		//1. If Tid = 1, input value of B
		if (Tid == 1)
		{
			input_Vector(B);
		}
		//1. If Tid = 2, input value of MO
		if (Tid == 2)
		{
			input_Matrix(MO);
		}
		//1. If Tid = 3, input value of MT, Z
		if (Tid == 3)
		{
			input_Vector(Z);
			input_Matrix(MT);
		}

		//2. Synchronization in the end of input
#pragma omp barrier

		//3. Copying local variables
		int di;
		Vector Bi(N);
		Matrix MRi(N, N);

		omp_set_lock(&key_Lock);
		di = d;
		omp_unset_lock(&key_Lock);
		
#pragma omp critical(critical_Sec)
		{
			Bi = B;
			MRi = MR;
		}

		//4. Calculating of the algorithm
		sub_Vectors(multiply_Matrix_Matrix_Vector(MT, MRi, multiply_Vector_Matrix(MO, Bi, Tid), Tid), multiply_Vector_Integer(Z, di, Tid), A, Tid);

		//5. Synchronization in the end of calculation
#pragma omp barrier

		//6. If Tid = 1, output A
		if (Tid == 1)
		{
			output_Vector(A);
		}

		cout << "T" << endstr << endl;
	}

	cout << "Labwork #4 finished" << endl;
	cin.get();

    return 0;
}

