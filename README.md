Laboratory work #4 for Programming for Parallel Computer Systems.

Purpose for the work: To study the Barrier, lock() and Critical section mechanism, that was made for programming library of parallel systems - OpenMP.
Programming language: C++.
Used technologies: From the programming library <omp.h> I have all kind of variables, functions and also pragmas to work with intercommunication between threads in the OpenMP program.

Controling of the program:
variable N stands for the size of Matrixes and Vectors respectively.
Program works for 4 threads, first one outputs the result.